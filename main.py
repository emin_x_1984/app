from flask import Flask, request, jsonify, json

app = Flask(__name__)

@app.route('/')
def main():
    return '<h1>This main page of our application <br></h1>'

@app.route('/users', methods=['GET', 'POST'])
def users():
    users = []
    user1 = {1: {"emin": "khozehagg"}}
    user2 = {2: {"kamil": "babayev"}}
    user3 = {3: {"famil": "babayev"}}
    user4 = {4: {"hesen": "babayev"}}
    users.append(user1)
    users.append(user2)
    users.append(user3)
    users.append(user4)
    return users

@app.route('/add_user', methods=['GET', 'POST'])
def add_user():
    return 'will be implemented'

if __name__ == "__main__":
    app.run(host='0.0.0.0')

